﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              ParseJSONIdocScriptFunction
	Created By:             Mythics
	Author:                 Jonathan Hult
	Last Updated On:        build_2_20120820

*******************************************************************************
** Overview
*******************************************************************************

	This component adds a new Idoc Script function entitled "parseJSON". It 
	retrieves a value from a JSON string for a given key. It takes two 
	parameters. The first parameter is a JSON string in the following format: 
	{"href":"https://www.google.com","target":"_blank"}
	The second parameter is the name of the key to retrieve (such as "href" 
	or "target").
	
	Example:
	<a href="<!--$parseJSON(wcmElement('Link'), 'href')-->" 
	target="<!--$parseJSON(wcmElement('Link'), 'target')-->">Google</a>
	
	Static tables:
	ParseJSONIdocScriptFunction_IdocScriptExtensions
	
	Tracing sections:
	idocscript
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	-10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************

build_1_20120820
	- Initial component release
