package com.mythics.webcenter.content.parsejsonidocscriptfunction;

import static com.mythics.webcenter.content.parsejsonidocscriptfunction.Constants.PARSE_JSON;
import static com.mythics.webcenter.content.parsejsonidocscriptfunction.Constants.TRACE_SECTION;
import static intradoc.common.GrammarElement.STRING_VAL;
import static intradoc.common.ScriptUtils.getDisplayString;
import static intradoc.server.script.ScriptExtensionUtils.computeReturnObject;
import intradoc.common.ExecutionContext;
import intradoc.common.LocaleUtils;
import intradoc.common.Report;
import intradoc.common.ScriptExtensionsAdaptor;
import intradoc.common.ScriptInfo;
import intradoc.common.ServiceException;
import intradoc.shared.UserData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Jonathan Hult
 */
	public class ParseJSONIdocScriptFunction extends ScriptExtensionsAdaptor {

	    public ParseJSONIdocScriptFunction() {
	        // List of functions
	        m_functionTable = new String[]{PARSE_JSON};

	        // Configuration data for functions.  This list must align with the "m_functionTable"
	        // list.  In order the values are "id number", "Number of arguments", "First argument type",
	        // "Second argument type", "Return Type".  Return type has the following possible
	        // values: 0 generic object (such as strings) 1 boolean 2 integer 3 double.
	        // The value "-1" means the value is unspecified.
	        m_functionDefinitionTable = new int[][]{
	            {0, 2, STRING_VAL, STRING_VAL, 0}, // parseJSON
	        };
	    }

	    /**
	     * This is where the custom IdocScript function is evaluated.
	     */
	    @Override
	    public boolean evaluateFunction(final ScriptInfo info, final Object[] args, final ExecutionContext context) throws ServiceException {

	        final int config[] = (int[]) info.m_entry;
	        final String function = info.m_key;

	        final int nargs = args.length - 1;
	        final int allowedParams = config[1];
	        if (allowedParams >= 0 && allowedParams != nargs) {
	            final String msg = LocaleUtils.encodeMessage("csScriptEvalNotEnoughArgs",
	                    null, function, "" + allowedParams);
	            throw new IllegalArgumentException(msg);
	        }

	        String msg = LocaleUtils.encodeMessage("csScriptMustBeInService", null, function, "Service");

	        final UserData userData = (UserData) context.getCachedObject("UserData");
	        if (userData == null) {
	            msg = LocaleUtils.encodeMessage("csUserDataNotAvailable", null, function);
	            throw new ServiceException(msg);
	        }

	        if (nargs == 2) {
	            if (config[2] == STRING_VAL && config[3] == STRING_VAL) {
	                // Retrieve first parameter which is the JSON string to parse
	                final String jsonString = getDisplayString(args[0], context);

	                // Retrieve second parameter which is the key
	                final String key = getDisplayString(args[1], context);

					try {
						// Object to store JSON
						final JSONObject jsonObject = new JSONObject(jsonString);

		                // Setup return value
		                args[nargs] = computeReturnObject(config[4], false, 1, 1, jsonObject.getString(key));
		                return true;
					} catch (final JSONException e) {
						final String message = "Error in retrieving key " + key + " for JSON string " + jsonString;
						Report.warning(TRACE_SECTION, message, e);
						return false;
					}
	            } else {
	                return false;
	            }
	        } else {
	            return false;
	        }
	    }
}
